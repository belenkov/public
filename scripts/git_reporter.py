#!/usr/bin/env python2.7
import requests
import sys
reload(sys)
sys.setdefaultencoding("UTF-8")


def reporter(target_branch, date_since, date_until, projects, api_key):

    print '  /$$$$$$  /$$$$$$ /$$$$$$$$       /$$$$$$$  /$$$$$$$$ /$$$$$$$   /$$$$$$  /$$$$$$$  /$$$$$$$$ /$$$$$$$$ /$$$$$$$         /$$$$$$   /$$$$$$  /$$$$$$$  /$$$$$$ /$$$$$$$  /$$$$$$$$'
    print ' /$$__  $$|_  $$_/|__  $$__/      | $$__  $$| $$_____/| $$__  $$ /$$__  $$| $$__  $$|__  $$__/| $$_____/| $$__  $$       /$$__  $$ /$$__  $$| $$__  $$|_  $$_/| $$__  $$|__  $$__/'
    print '| $$  \__/  | $$     | $$         | $$  \ $$| $$      | $$  \ $$| $$  \ $$| $$  \ $$   | $$   | $$      | $$  \ $$      | $$  \__/| $$  \__/| $$  \ $$  | $$  | $$  \ $$   | $$   '
    print '| $$ /$$$$  | $$     | $$         | $$$$$$$/| $$$$$   | $$$$$$$/| $$  | $$| $$$$$$$/   | $$   | $$$$$   | $$$$$$$/      |  $$$$$$ | $$      | $$$$$$$/  | $$  | $$$$$$$/   | $$   '
    print '| $$|_  $$  | $$     | $$         | $$__  $$| $$__/   | $$____/ | $$  | $$| $$__  $$   | $$   | $$__/   | $$__  $$       \____  $$| $$      | $$__  $$  | $$  | $$____/    | $$   '
    print '| $$  \ $$  | $$     | $$         | $$  \ $$| $$      | $$      | $$  | $$| $$  \ $$   | $$   | $$      | $$  \ $$       /$$  \ $$| $$    $$| $$  \ $$  | $$  | $$         | $$   '
    print '|  $$$$$$/ /$$$$$$   | $$         | $$  | $$| $$$$$$$$| $$      |  $$$$$$/| $$  | $$   | $$   | $$$$$$$$| $$  | $$      |  $$$$$$/|  $$$$$$/| $$  | $$ /$$$$$$| $$         | $$   '
    print ' \______/ |______/   |__/         |__/  |__/|________/|__/       \______/ |__/  |__/   |__/   |________/|__/  |__/       \______/  \______/ |__/  |__/|______/|__/         |__/   '


    print ' __  __  __     __ _____ __       '
    print '|__)|__)/  \  ||_ /   | (_        '
    print '|   | \ \__/__)|__\__ | __)       %s' % sys.argv[4]

    print ' __  __          __               '
    print '|__)|__) /\ |\ |/  |__|           '
    print '|__)| \ /--\| \|\__|  |           %s' % sys.argv[1]

    print ' __      __ __                    '
    print '(_ ||\ |/  |_                     '
    print '__)|| \|\__|__                    %s' % sys.argv[2]
    print '      ___                         '
    print '| ||\| |  | |                     '
    print '|_|| | |  | |__                   %s\n' % sys.argv[3]


    headers = {'PRIVATE-TOKEN': api_key}
    project_list = []

    def project_in_group(project_group_id):

        url_group_subgroups = 'https://gitlab/api/v4/groups/%s/subgroups?per_page=100' % project_group_id
        subgroup_list = requests.get(url_group_subgroups, headers=headers).json()
        if isinstance(subgroup_list, list) and subgroup_list:
            for sg in subgroup_list:
                if not sg.get("message"):
                    project_in_group(sg[u'id'])
        else:
            if isinstance(subgroup_list, dict) and subgroup_list.get("message"):
                return
            url_group_project = 'https://gitlab/api/v4/groups/%s?per_page=100' % project_group_id

            return project_list.extend(requests.get(url_group_project, headers=headers).json()['projects'])

# IS IT GROUP
    group_list_url = 'https://gitlab/api/v4/groups'
    group_list = requests.get(group_list_url, headers=headers).json()

    for gr in group_list:
        if gr['name'] in projects.split(','):
            project_in_group(gr['id'])

# IS IT PROJECT
    project_url = 'https://gitlab/api/v4/projects?per_page=100'
    project_page_count = page_checker(7, project_url, headers)
    for project_page in range(1, project_page_count + 1):
        paginated_project_url = project_url + '&page=%d' % project_page
        proj_list = requests.get(paginated_project_url, headers=headers)

        for repo in proj_list.json():
            if (repo['name'] in projects.split(',')) or (repo['path_with_namespace'] in projects.split(',')) :
                project_list.append(repo)

# GET REPO
    for repo_result in project_list:

        print ' __  __ __  __  __ ___ __  __     '
        print '|__)|_ |__)/  \(_ | | /  \|__)\_/ '
        print '| \ |__|   \__/__)| | \__/| \  |  %s\n' % repo_result['path_with_namespace']
# GET BRANCH
        url_branches = 'https://gitlab/api/v4/projects/%s/repository/branches?per_page=100' % repo_result['id']
        branch_list = []
        branch_page_count = page_checker(3, url_branches, headers)

        for branch_page in range(1, branch_page_count + 1):
            paginated_branch_url = url_branches + '&page=%d' % branch_page
            branch_list += requests.get(paginated_branch_url, headers=headers).json()

        for branch in branch_list:
            if target_branch == branch['name']:
                branch_result = branch
# GET COMMITS
                url_commits = 'https://gitlab/api/v4/projects/%s/repository/commits?ref_name=%s&since=%sT00:00:00.000+03:00&until=%sT00:00:00.000+03:00&per_page=100' % (repo_result['id'], branch_result['name'], date_since, date_until)
                commit_page_count = page_checker(7, url_commits, headers)
                if not requests.get(url_commits, headers=headers).json():
                    print "No changes"
                for commit_page in range(1, commit_page_count + 1):
                    paginated_url = url_commits + '&page=%d' % commit_page
                    commit_list = requests.get(paginated_url, headers=headers)
                    for commit in commit_list.json():
                        if "Merge branch" in commit['message']:
                            continue
                        else:
                            print 'Commit: %s\nCreated at: %s' % (commit['id'], commit['created_at'])
                            print 'Author: %s\nCommit msg: %s\nChanged files:\n' % (commit['committer_name'], commit['title'])
# COMPARE TWO COMMITS

                        if not len(commit['parent_ids']):
                            continue
                        else:
                            url_comparisons = 'https://gitlab/api/v4/projects/%s/repository/compare?from=%s&to=%s&per_page=100' % (repo_result['id'], commit['parent_ids'][0], commit['id'])
                            for committed_files in requests.get(url_comparisons, headers=headers).json()['diffs']:
                                if committed_files['new_path'] == committed_files['old_path']:
                                    print '    Path: %s' % committed_files['new_path']
                                else:
                                    print '    0ld path: %s; new path: %s' % (committed_files['old_path'], committed_files['new_path'])

                            print '\n-------------------------------------------------------'
                break
            else:
                continue

    print ' __       __     __ __               '
    print '|_ ||\ ||(_ |__||_ |  \              '
    print '|  || \||__)|  ||__|__/              '

    return True



def page_checker(step, url, headers, tmp_page_value=0):
    if not tmp_page_value:
        tmp_page_value = step

    paginated_url = url + '&page=%d' % tmp_page_value
    paginated_url_plus_one = url + '&page=%d' % (tmp_page_value + 1)

    if step > 1:
        step /= 2

    if not len(requests.get(paginated_url, headers=headers).json()) and tmp_page_value == 1:
        return 0
    elif not len(requests.get(paginated_url, headers=headers).json()):
        return page_checker(step, url, headers, tmp_page_value - step)
    elif not len(requests.get(paginated_url_plus_one, headers=headers).json()) and len(requests.get(paginated_url, headers=headers).json()):
        return tmp_page_value
    else:
        return page_checker(step, url, headers, tmp_page_value + step)

if __name__ == "__main__":
    reporter(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5])
