#!/usr/bin/python3

import requests
from requests.auth import HTTPBasicAuth
import re

auth = HTTPBasicAuth('%USER%', '%PASS%')

current_image = requests.get(
    'http://%PROM_ADDR%:9090/api/v1/query?query=avg(pod_info{image=~"%REGISTRY_ADDR%.*",status="Running",imageID!=""})by(image)'
    )

result = {}
for с in current_image.json()['data']['result']:
    try:
        result.setdefault(
            re.findall(r'%REGISTRY_ADDR%/(.*):', с['metric']['image'])[0],
            []
            ).append(re.findall(r'(\d+\.\d+\.\d+(.\d+)*-\w*\.\d+)', с['metric']['image'])[0][0])

    except IndexError:
        result.pop(re.findall(r'%REGISTRY_ADDR%/(.*):', с['metric']['image'])[0])

print('Current images ' + str(result))

for app in result:
    tags = requests.get('https://%NEXUS_ADDR%/nexus/repository/docker-test/v2/{repo}/tags/list'.format(repo=app), auth=auth).json()['tags']

    for tag in tags:
        if tag not in result[app]:
            digest = requests.get(
                'https://%NEXUS_ADDR%/nexus/repository/docker-test/v2/{repo}/manifests/{tag}'.format(repo=app, tag=tag),
                auth=auth,
                headers={"Accept":"application/vnd.docker.distribution.manifest.v2+json"}
            ).headers['Docker-Content-Digest']

            if requests.delete('https://%NEXUS_ADDR%/nexus/repository/docker-test/v2/{repo}/manifests/{digest}'.format(repo=app, digest=digest), auth=auth).ok:
                print('Image {repo}:{tag} with digest {digest} deleted'.format(repo=app, tag=tag, digest=digest))
