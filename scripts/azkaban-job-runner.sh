#!/bin/bash
##TODO: case для выбора выкачки метрик бомбора, тд, сф, нетфактор
##TODO: передавать параметры скрипту , а не объявлять их в коде
#INPUN DATA

USERNAME=""
PASSWORD=""
HOSTADDR=""
PORT=""

PROJECT=(Metrics-Download-All Metrics-Download-KW-CXT Metrics-Download-IP-LOC-DMN ftp_upload)
#PROJECT=(bombora)
#FLOW=(upload_all-2day)
#PARAMETER=(TARGET_DATE)
FLOW=(today-6days today-6days today-6days ftp-upload-5day)
PARAMETER=(reportDate reportDate reportDate TARGET_DATE)

##Перечисляем даты либо списком (раскомментить строку ниже)
#PARAMVALUE=(2018-08-01 2018-08-02 2018-08-03 2018-08-04 2018-08-05 2018-08-06 2018-08-07 2018-08-08 2018-08-09 2018-08-10 2018-08-11 2018-08-12 2018-08-13 2018-08-14 2018-08-15 2018-08-16 2018-08-17 2018-08-18 2018-08-19 2018-08-20 2018-08-21 2018-08-22 2018-08-23 2018-08-24 2018-08-25 2018-08-26 2018-08-27 2018-08-28 2018-08-29 2018-08-30 2018-08-31)
#PARAMVALUE=(2018-05-14)

##Либо с помощью цикла
#Указываем начальную дату (2017-05-01), а так же количество дней подряд, отсчет начиначется с 0
#Если значение +$i - скрипт движется в сторону возростания дат, если же -$i наоборот
for i in {0..61}; do PARAMVALUE+=( $(date -I -d "2016-07-09 +$i days") ); done

LOGFILE=$(tempfile -d ./ -n job-runner-$(date '+%Y-%m-%dT%H:%M:%S').log)
echo "log file $LOGFILE was created"


### AUTH FUNCTION
function azkaban_auth {
        AUTHRESP=$(curl -sk -X POST --data "action=login&username=$USERNAME&password=$PASSWORD" -k https://$HOSTADDR:$PORT)
        echo "Connection $(echo $AUTHRESP | jq -r '.status' )"
        SESSIONID=$(echo $AUTHRESP | jq -r '."session.id"')
        if [ $1 == "reconnect" ]; then
                echo "Session $TEMPSESSIONID expired. We reconnected to azkaban with a new session $SESSIONID" # Уведомляем о заэкспайренной сессии
                echo "Session $TEMPSESSIONID expired. We reconnected to azkaban with a new session $SESSIONID" >> $LOGFILE
        fi
}


### CALL AUTH FUNCTION
azkaban_auth 0

### START
echo $AUTHRESP >> $LOGFILE
echo ${#PARAMVALUE[@]}
for ((j=0;j<${#PARAMVALUE[@]};j++)); do
        echo "Start date ${PARAMVALUE[j]}"
        echo "Start date ${PARAMVALUE[j]}" >> $LOGFILE
        for ((i=0;i<${#PROJECT[@]};i++)); do
                echo "$(date '+%Y-%m-%dT%H:%M:%S'): ${PROJECT[i]} ${FLOW[i]}"

                STARTRESP=$(curl -sk --get --data "session.id=$SESSIONID" --data "ajax=executeFlow" --data "project=${PROJECT[i]}" --data "flow=${FLOW[i]}" --data "flowOverride[${PARAMETER[i]}]=${PARAMVALUE[j]}" https://$HOSTADDR:$PORT/executor)

                ### CHECK IF SESSION ID EXPIRED
                # Message when session expired
                # "error" : "session"
                #
                TEMPSESSIONID=$SESSIONID
                STATUSAUTH=$(echo $STARTRESP | jq -r '.error')
                if [ "$STATUSAUTH" == "session" ]; then
                        ### AUTH AGAIN; i.e. SESSIONID var will changed
                        azkaban_auth "reconnect"
                        ### START FLOW AGAIN
                        STARTRESP=$(curl -sk --get --data "session.id=$SESSIONID" --data "ajax=executeFlow" --data "project=${PROJECT[i]}" --data "flow=${FLOW[i]}" --data "flowOverride[${PARAMETER[i]}]=${PARAMVALUE[j]}" https://$HOSTADDR:$PORT/executor)
                fi

                echo $STARTRESP
                echo $STARTRESP >> $LOGFILE

                EXECID=$(echo $STARTRESP | jq -r '.execid')
                STATUS="TEMP"
                while [ "$STATUS" != "SUCCEEDED" ]; do
                        sleep 120
                        FETCHRESP=$(curl -sk --data "session.id=$SESSIONID&ajax=fetchexecflowupdate&execid=$EXECID&lastUpdateTime=-1" https://$HOSTADDR:$PORT/executor)
                        ## We need to catch expired session in this while too
                        HTMLFETCHRESP=$(echo $FETCHRESP | grep "DOCTYPE") # If session expired, FETCHRESP return HTML login page
                        TEMPSESSIONID=$SESSIONID # Save current session.id. If expired it's need to notify in log
                        if [ "$HTMLFETCHRESP" ]; then # If session is not expired, HTMLFETCHRESP = FALSE and go to check status
                                # AUTH AGAIN; i.e. SESSIONID var will changed
                                azkaban_auth "reconnect"
                                # FETCH AGAIN
                                FETCHRESP=$(curl -sk --data "session.id=$SESSIONID&ajax=fetchexecflowupdate&execid=$EXECID&lastUpdateTime=-1" https://$HOSTADDR:$PORT/executor)
                        fi
                        # If everything is OK, upper IF construction will skipped
                        STATUS=$(echo $FETCHRESP | jq -r '.status')
                        echo "status $STATUS"
                        if [ "$STATUS" != "RUNNING" ]; then
                                if [ "$STATUS" != "PREPARING" ]; then
                                        if [ "$STATUS" != "SUCCEEDED" ]; then
                                        echo $FETCHRESP >> $LOGFILE
                                                break
                                        fi
                                fi
                        fi
                done
                echo $FETCHRESP >> $LOGFILE
                if [ "$STATUS" != "RUNNING" ]; then
                        if [ "$STATUS" != "PREPARING" ]; then
                                if [ "$STATUS" != "SUCCEEDED" ]; then
                                        echo "Problem with ${PROJECT[i]} ${FLOW[i]} for ${PARAMVALUE[j]}"
                                        break
                                fi
                        fi
                fi
        done
        echo "$(date '+%Y-%m-%dT%H:%M:%S'): Date ${PARAMVALUE[j]} complite"
done
echo "SUCCEEDED"
exit 0
