#!/usr/bin/python3

import argparse
import time
import requests
import sys
import prometheus_client
from prometheus_client import start_http_server, Info, Gauge


class AppMetrics:

    def __init__(self):
        self.pod_info = Info('pod', 'application info', ['app', 'image', 'imageID', 'namespace', 'pod_name', 'status'])
        self.restart_count = Gauge('pod_restart_count', 'restart number of application pod', ['namespace', 'pod_name'])
        parser = self.init_argparse()
        self.args = parser.parse_args()

    def init_argparse(self):
        parser = argparse.ArgumentParser(
            usage="%(prog)s [OPTION] [FILE]...",
            description="Gets a list of used images into k8s."
        )
        parser.add_argument(
            "-t", "--token", required=True,
            help="k8s API token"
        )
        #telemetry.address
        parser.add_argument(
            "-p", "--port", default=9119, type=int,
            help="exporter listen port. Default is 9119"
        )
        parser.add_argument(
            "-a", "--address", required=True,
            help="apiserver address"
        )
        parser.add_argument(
            "-i", "--interval", default=600, type=int,
            help="polling interval seconds. Default is 600"
        )
        parser.add_argument(
            "-c", "--ca_certificate", default=False,
            help="k8s ca-certificate. Default is False"
        )
        return parser

    def metrics_loop(self):

        while True:
            self.fetch()
            time.sleep(self.args.interval)

    def fetch(self):

        r = requests.get("{apiserver}/api/v1/pods".format(apiserver=self.args.address), headers={"Authorization": "Bearer {token}".format(token=self.args.token)}, verify=self.args.ca_certificate)

        for pod in r.json()['items']:
            for container in pod['status']['containerStatuses']:
                metric_name = pod['metadata']['namespace'].replace('-', '_') + '_' + container['name'].replace('-', '_')

                if not any( metric.name == metric_name for metric in prometheus_client.REGISTRY.collect() ):
                    self.pod_info.labels(
                        app = container['name'],
                        image = container['image'],
                        imageID = container['imageID'],
                        namespace = pod['metadata']['namespace'],
                        pod_name = pod['metadata']['name'],
                        status = pod['status']['phase']
                    )
                    self.restart_count.labels(
                        namespace = pod['metadata']['namespace'],
                        pod_name = pod['metadata']['name']
                    ).set(container['restartCount'])

def main():

    app_metrics = AppMetrics()
    start_http_server(app_metrics.args.port)
    app_metrics.metrics_loop()

if __name__ == "__main__":
    main()


